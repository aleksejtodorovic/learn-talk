var ignoredTags = ['SCRIPT', 'HEADER', 'FOOTER', 'VIDEO', 'SVG', 'AUDIO', 'IFRAME', 'IMG', 'META', 'LINK', 'A', 'BODY', 'HEAD'],
    goodTags = ['P', 'SPAN', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6'],
    wordNodeMap = [],
    keywords = [],
    nodeXOffset = 0,
    nodeYOffset = 0,
    currentWord = 0;

function isIgnoredTag (node) {
    try {
        if (node.tagName && ignoredTags.indexOf(node.tagName) > -1) {
            return true;
        }
        return false;
    } catch(ex) {
        console.log(ex);
        return false;
    }
}

function isGoodTag (node) {
    try {
        if(node.tagName && goodTags.indexOf(node.tagName) > -1) {
            return true;
        }

        return false;
    } catch(ex) {
        console.log(ex);
        return false;
    }
}

function splitWords(node){
    var fullString = node.innerText || node.textContent;

    return fullString.replace(/([ .,:;!?"()/=\d\s]+)/gi, "|").split('|');
}

function existsAsKeyword(word){
    if(wordNodeMap[word] || keywords.indexOf(word) !== -1) {
        return true;
    }

    return false;
}

function changeWordStyle(nodeOfWord, keyword) {
    var fullString = nodeOfWord.innerText || nodeOfWord.textContent,
        resto = fullString.split(keyword, 2),
        finalString = '';

    finalString += resto[0];
    finalString += '<span id=\'word' + currentWord + '\' class=\'markedWord\'>' + keyword + '</span>';
    finalString += resto[1];

    nodeOfWord.innerHTML = finalString;

    var container = document.getElementById('word' + currentWord++);
    container.addEventListener('mouseover', placeTranslate);
    container.addEventListener('mouseout', removeTranslate);

}

function searchForWords(node) {
    var childNodes = node.childNodes;

    childNodes.forEach((currentChild, index) => {
        if(!isIgnoredTag(currentChild)) {
            if (currentChild.nodeType === 1 && isGoodTag(currentChild)) {
                var words = splitWords(currentChild);

                words.forEach((word, index) => {
                    if (word.length > 2 && !existsAsKeyword(word)) {
                        wordNodeMap[word] = currentChild;
                        keywords.push(word);
                    }
                });
            } else if(currentChild.nodeType === 1) {
                searchForWords(currentChild);
            }
        }
    });
}

function calculateOffsets(node) {
    var obj = node;
    nodeXOffset = 0;
    nodeYOffset = 0;

    if (obj.offsetParent) {
        do {
            nodeXOffset += obj.offsetLeft;
            nodeYOffset += obj.offsetTop;

        } while (obj = obj.offsetParent);
    }
}

function placeTranslate(event) {
    var targetedWord = event.target || event.srcElement,
        translate = document.createElement('div');

    /*const trans = require('google-translate-api');

    trans('Ik spreek Engels', {to: 'en'}).then(res => {
        console.log(res.text);
        //=> I speak English
        console.log(res.from.language.iso);
        //=> nl
    }).catch(err => {
        console.error(err);
    });
    */

    calculateOffsets(targetedWord);

    translate.id = 'myTestDiv';
    translate.style.width = '100px';
    translate.style.height = '100px';
    translate.style.background = 'red';
    translate.style.position = 'absolute';
    translate.style.left = nodeXOffset + 'px';
    translate.style.top = (nodeYOffset + targetedWord.offsetHeight) + 'px';

    document.body.appendChild(translate);
}

function removeTranslate(){
    setTimeout(function(){
        try {
            var translate = document.getElementById('myTestDiv');

            document.body.removeChild(translate);
        } catch(ex){
            console.log(ex);
        }
    }, 1000);
}

(function run() {
    searchForWords(document.body);

    for(let i = 0; i < 10; ++i){
        var randomNumber = Math.floor(Math.random() * keywords.length),
            keyword = keywords[randomNumber],
            nodeOfWord = wordNodeMap[keyword];

        changeWordStyle(nodeOfWord, keyword);
    }
})();
